#!/bin/sh

MESSAGE_DB_VERSION=$(cat version.txt)

cd /usr/src/eventide/message-db-${MESSAGE_DB_VERSION}/database
./install.sh
