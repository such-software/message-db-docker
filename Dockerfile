FROM postgres:15-alpine

RUN apk add --no-cache curl tar
ARG MESSAGE_DB_VERSION
RUN mkdir -p /usr/src/eventide \
  && curl -L https://github.com/message-db/message-db/archive/refs/tags/v$MESSAGE_DB_VERSION.tar.gz -o /usr/src/eventide/message-db.tgz
RUN tar -xf /usr/src/eventide/message-db.tgz --directory /usr/src/eventide

COPY rundbscripts.sh /docker-entrypoint-initdb.d/
COPY version.txt /

ENV PGDATA /data
ENV POSTGRES_HOST_AUTH_METHOD trust
RUN docker-entrypoint.sh postgres --version

ENTRYPOINT docker-entrypoint.sh postgres

