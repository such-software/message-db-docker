#!/bin/bash

docker buildx build \
  --build-arg="MESSAGE_DB_VERSION=$(cat version.txt)" \
  --platform linux/arm64,linux/amd64 \
  --tag ethangarofolo/message-db:latest \
  --tag ethangarofolo/message-db:$(cat image-version.txt) \
  --push \
  .
