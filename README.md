# message-db-docker

A Docker image with [Message DB's](https://github.com/message-db/message-db) PostgreSQL message store implementation.  Does not install a Ruby gem---just gets the installation files directly from GitHub.

## When updating the Message DB version

Be sure to change the value in `version.txt` to whatever the Message DB version you're building is

Run `./publish.sh` to build and publish.

## Acknowledgements

A major thanks to GitHub user bobisme for your [eventide-docker](https://github.com/bobisme/eventide-docker).

And of course to the [Eventide Project](https://eventide-project.org/) for Message DB.

## License

MIT
